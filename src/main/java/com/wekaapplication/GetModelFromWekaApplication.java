package com.wekaapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GetModelFromWekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(GetModelFromWekaApplication.class, args);
		TestController.loadBikeModel();
		TestController.loadcarModel();
	}
}
